# RWBot
Bot created to automate the annual Toradora rewatch on /r/anime.

## Requirements
Python 3.5+

`praw`

## Setup for a new run(assuming it'll be run on a free account on PythonAnywhere)

1. Update the fanart and sources files according to the following.

    1a. If you don't want to post fanart in the threads, leave the "fanart.txt" and "sources.txt" files blank.

    1b. Update the fanart by putting a link to it in the corresponding line of "fanart.txt" and its source in "sources.txt"
    Example: In case you're setting up the fanart for episode 25, the 25th line of fanart.txt should be
            `[Image link](i.imgur.com/linktomyimage)` and the 25th line on "sources.txt" should be `[Source to my image](LinkToSourceOfMyImage.com)`

2. Update the questions.txt file. Leave it blank if undesired.

3. Configure the app credentials on the config.ini file and set "post_unlocked" to 1.

4. Setup the task on PythonAnywhere to the desired time.

Note: In the past, there have been issues with PythonAnywhere in which the user's account apparently persists in not using UTF-8. Due to that, it is recommended to set the tasks's command to something like `LC_CTYPE=en_US.utf-8 python3 /home/<USERNAME>/run.py` to be sure.

## Setup for a test run

1. The bot is equiped to run a simulated rewatch on r/ToradoraBotTest. In order to do so, simply add a `1` as an argument to the command like this: `LC_CTYPE=en_US.utf-8 python3 /home/<USERNAME>/run.py 1`
