import linecache
import configparser
from datetime import date

line = "\n___\n\n"

config = configparser.ConfigParser()
config.read('./configuration/config.ini')

# Returns the title of the episode for the first line of the post
def get_episode_title(n):
    return read_nth_line("./files/titles.txt", n).strip()+line

# Returns the statement section of the post warning of spoilers ,etc.
# A different statement is set for the last post of the rewatch
def get_statement(n):
    filename = "./configuration/standard_statement.txt"
    if n==26:
        filename = "./configuration/last_statement.txt"
    with open(filename, "r") as file:
        return ''.join(file.readlines()) + line

# Returns Posting Time section from the configuration file
# The posting time will not be published on the last thread of the rewatch
def get_thread_time(number):
        if number==26:
            return ''
        else:
            post_time = config.get('post_section', 'post_time')
            return "Threads will be posted daily at: "+post_time + line

# Returns the Streams section of the post
def get_streams():
    stream_list = "[CR](http://www.crunchyroll.com/toradora), " \
                  "[Netflix](https://www.netflix.com/title/80049275), " \
                  "[Amazon Prime](https://www.amazon.com/Toradora/dp/B07L7BYYXC), " \
                  "[Hulu](http://www.hulu.com/toradora), " \
                  "[Viewster](http://www.viewster.com/serie/1321-19573-000/toradora/), " \
                  "[Yahoo](https://view.yahoo.com/show/toradora)"
    return stream_list + line

# Returns a formatted link to an episode
def episode_link(n, link):
    link = link.strip()
    return "[Episode "+str(n)+"]("+link+")" if n < 26 else "[OVA]("+link+")"

# Fills out and returns table with episode links. Current Episode will be in plain text to avoid redundancy
def get_table(n):
    cur_year = date.today().year
    
    # Header of the table
    header = "This Year's Discussion ("+str(cur_year)+") | Last Year's Discussion ("+str(cur_year-1)+") |"\
             + "\n:---|:---|\n"
    
    #Adds header to the table
    table = header
    
    #table.txt refers to the file with the links to the previous year's rewatch.
    #current.txt refers to the file where the links to the current rewatch are kept
    with open("./files/table.txt", "r") as previous, open("./files/current.txt", "r") as current:
        for x in range(n):
            # This will get a link to both the episode and last year's discussion of the same episode
            if x != n-1:
                link_current_year = episode_link(x+1, current.readline())
                link_previous_year = episode_link(x+1, previous.readline())
                
                #Adds line to the table
                table += link_current_year + " | " + link_previous_year + "\n"
            # Fills out the current day's line of the table. The current day's thread will not be linked to avoid redundancy
            else:
                link_previous_year = episode_link(x + 1, previous.readline())
                link_current_year = "Episode "+str(n) if n < 26 else "OVA"
                
                #Adds last line to the table
                table += link_current_year + " | " + link_previous_year

    #Returns the formatted table with a separator line after
    return table+line

# Returns the Fanart section of the post
def get_fanart(n):
    #Fetches the current day's fanart according to the fanart.txt file
    #Each day's fanart section should be written in a single line of the file. Days without fanart should be left as a blank line
    fanart = read_nth_line("./configuration/fanart.txt", n)
    
    #If there's fanart to be posted it will strip the line of unecessary spaces and return the formatted fanart section. If not, the section won't be posted.
    if fanart:
        if fanart.strip():
            return "Fanart:\n\n" + fanart + line
    else:
        return ''

# Returns the Fanart Sources section of the post
def get_sources(n):
    #Fetches the current day's fanart source according to the sources.txt file
    #Each day's Source section should be written in a single line of the file. Days without fanart (and their sources) should be left as a blank line
    sources = read_nth_line("./configuration/sources.txt", n)
    
    #If there's fanart sources to be posted it will strip the line of unecessary spaces and return the formatted fanart sources section. If not, the section won't be posted.
    if sources:
        if sources.strip():
            return "Sources:\n\n" + sources+line
    else:
        return ''

#Returns the Question of the Day section of the post
def get_question_of_the_day(n):
    #Fetches the Question of the day according to the questions.txt file
    #Each day's Question of the Day section should be written in a single line of the file. Days without Question of the Day should be left as a blank line
    question = read_nth_line("./configuration/questions.txt", n)

     #If there's a question of the day to be posted it will strip the line of unecessary spaces and return the formatted question of the day section. If not, the section won't be posted.
    if question:
        if question.strip():
            return "Feel free to participate in our bonus topic at the end of your comment or separately:\n\n* **Christmas Club Bonus!** " + question
    else:
        return ''

# Method used to get the episode titles, questions of the day, fanarts and sources from their respective files
def read_nth_line(name, n):
    if(len(open(name).readlines(  ))):
        with open(name, 'r', encoding="utf-8") as file:
            return file.read().splitlines()[n-1]

# Returns entire thread's body by using the methods for each section
def get_body(n):
    body = get_episode_title(n)+\
           get_statement(n)+\
           get_thread_time(n)+\
           get_streams()+\
           get_table(n)+\
           get_fanart(n)+\
           get_sources(n)+\
           get_question_of_the_day(n)

    return body
