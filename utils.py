import os

qt_episodes = 26

# Returns an array containing the title and text of the PM to be sent whenever an episode is posted
def get_success_message(n):
    message = "I've successfully posted thread for episode {0}.".format(n)
    #When the rewatch ends, the bot sends a special message
    if n == qt_episodes:
        message += "\n\n[Now i'll deactivate and go back to that dark, cold place...](" \
                   "https://i.imgur.com/ziHzZ4x.gifv)"
    #This episode is sad so the bot is sad as well
    elif n == 19:
        message += "  [T_T](https://i.imgur.com/BJVppvD.gif)"

    return ["Thread posted successfully", message]

# Returns an array containing the title and body of the PM to be sent in case there's an error
def get_error_message(users):
    message = "There was an error while posting the thread. This message has been sent to "
    for user in users[:-1]:
        message += "," + user
    message += " and " + users[-1]
    return ["ERROR CHRISTMAS CLUB", message]

# Adds a link to the most recent thread to the list of links of the current rewatch
def add_current_to_file(current):
    with open("./files/current.txt","a") as file:
        current = current+"\n"
        file.write(current)

# Updates the episode counter. After reaching the last, it'll prepare the files for the next rewatch
def update_counter(number):
    number = int(number)
    with open("./files/counter.txt", 'w') as file:
        if number != qt_episodes:
            number += 1
            file.write(str(number))
        else:
            reset()

# Prevents bot from posting after OVA thread is posted. Can be unlocked again through the configuration file
# The reason this is necessary is that, since PythonAnywhere doesn't allow for free accounts to precisely schedule it's tasks, the bot would continue to post again even after the last episode is posted unless the user manually prevents the task from continuing.
def lock():
	config.set('post_section', 'post_unlocked', '0')
	with open('./configuration/config.ini', 'w') as file:
		config.write(file)

# Prepares the files for the next rewatch
def reset():
    #Switches out the previous year with the current one to prepare next year's rewatch
    os.remove("./files/table.txt")
    os.rename("./files/current.txt", "files/table.txt")
    #Resets counter to 1 and creates a fresh file for the next rewatch's links
    with open("./files/counter.txt", 'w') as file , open("files/current.txt", 'a'):
        file.write("1")
