from datetime import date
# Returns the episode number formatted for the title in a string
def get_title_number(n):
    return "Episode " + str(n) if n!=26 else "OVA"

# Return the title of the thread
def get_title(number):
    year = date.today().year
    return "[Spoilers] Toradora! Christmas Club Rewatch ("+str(year)+") " + get_title_number(number) + " Discussion"
