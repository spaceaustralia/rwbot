import rwbot
import sys
#There should be a 1 as an argument in order to initiate a test run over at r/ToradoraBotTest
if len(sys.argv)>1:
    rwbot.main(int(sys.argv[1]))
else:
    rwbot.main()
