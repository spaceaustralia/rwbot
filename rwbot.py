import praw
from configparser import SafeConfigParser
import body, utils, title

config = SafeConfigParser()
config.read('./configuration/config.ini')

# Logs into Reddit and returns a Reddit instance
def login():
    return praw.Reddit(
		username=config.get('user_section', 'username'),
		password=config.get('user_section', 'password'),
		client_id=config.get('user_section', 'client_id'),
		client_secret=config.get('user_section', 'client_secret'),
		user_agent=config.get('user_section', 'user_agent'))

# Submits the thread to /r/anime and returns the post in order to update the links for the current rewatch on the table
def post(reddit, title, text, test = 0):
    if test:
        return reddit.subreddit("ToradoraBotTest").submit(title, selftext=text, send_replies=False)
    else:
        return reddit.subreddit("anime").submit(title, selftext=text, send_replies=False, flair_id='bfd20a82-7ef5-11e8-abc8-0eb61748988c', flair="Rewatch")

# This will get the current episode count from counter.txt and return it after updating the count
# reddit Parameter serves only to send PMs in case of IOError
def get_number(reddit):
	try:
		with open("./files/counter.txt", 'r') as file:
			return int(file.read())

# In case there's an unexpected error reading the file, it'll send a message to all users defined in config.ini
	except IOError:
		send_message(reddit, utils.get_error_message(config.get('post_section', 'users').split(",")))
		return -1

def get_title(number):
    return title.get_title(number)

# Check body.py for information
def get_body(number):
    return body.get_body(number)

# Sends PMs to all users on the user list on the configuration file
def send_message(reddit, message):
	user_list = config.get('post_section', 'users')
	if user_list.strip():
	    message_title = message[0]
	    message_text = message[1]
	    for user in user_list.split(","):
		    reddit.redditor(user).message(message_title, message_text)

def main(test = 0):
    #Logins into reddit
    reddit = login()
    
    #If it's a test run, it'll run a test at r/ToradoraBotTest
    #For simplicity, the test run is not expected to have functioning links on it's table as the test run's links will not be saved anywhere
    if test:
        for x in range(1,27):
            current = post(reddit, get_title(x), get_body(x), test)
        return;

    #The bot is set to only post if the config file is set as "unlocked". See utils.py's lock method for more information
    elif int(config.get('post_section', 'post_unlocked'))==1:
        number = get_number(reddit)
        
        #Posts file and keeps the post in order to flair it and save it's shortlink
        current = post(reddit, get_title(number), get_body(number))
        utils.add_current_to_file(current.shortlink)
        
        #Updates the episode counter and resets the bot after the last episode
        utils.update_counter(number)
        send_message(reddit, utils.get_success_message(number))
        if number==26:
            utils.lock()
    else:
        print("LOCKED")
